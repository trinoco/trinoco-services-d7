<?php
// databaseresource.inc

function _databaseresource_index($page, $parameters) {
  global $databases;
  $settings = variable_get('trinoco-services-settings');

  if (isset($settings['export']) && $settings['export'] == 'false') {
    $return = array(array('database' => 'Not enabled for exporting.'));
  }else if (!file_exists('public://trinoco-services-' . $databases['default']['default']['database'] . '.sql.gz')) {
    $return = array(array('database' => 'Not exported yet.'));
  }
  else {
    $files = file_load_multiple(array(), array('uri' => 'public://trinoco-services-' . $databases['default']['default']['database'] . '.sql.gz'));
    $file = reset($files);
    $return  = array(array('database' => array('timestamp' => $file->timestamp, 'uri' => file_create_url($file->uri))));
  }

  return trinoco_services_utf8_converter($return);
}

/**
* Access callback for the note resource.
*
* @param string $op
*  The operation that's going to be performed.
* @param array $args
*  The arguments that will be passed to the callback.
* @return bool
*  Whether access is given or not.
*/

function _databaseresource_access($op) {
  global $user;
 
  return user_access('administer software updates', $user);
}
