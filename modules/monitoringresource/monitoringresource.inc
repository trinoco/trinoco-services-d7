<?php
// monitorringresource.inc

function _monitoringresource_index($page, $parameters) {
  global $databases;
  $result = db_query("SELECT table_name, (data_length+index_length)/1048576 as data_used,
  SUM(TABLE_ROWS) as row_count
  FROM information_schema.tables
  where table_schema = '" . $databases['default']['default']['database'] . "'
  GROUP BY TABLE_NAME
  ORDER BY (data_length+index_length+data_free) DESC");

  $db_usage = 0;
  $tables = array();
  foreach ($result as $record) {
    $db_usage += $record->data_used;
    $tables[$record->table_name] = array(
      'Used data' => $record->data_used,
      'rows' => $record->row_count
    );
  }
  
  $mysqli = new mysqli($databases['default']['default']['host'], $databases['default']['default']['username'], $databases['default']['default']['password']);
  $return = array(
    'system' => array(
      'memory_limit' => ini_get('memory_limit'),
      'memory_usage' => round(get_server_memory_usage(), 2) . 'm',
      'processor_usage' => get_server_cpu_usage() . '%',
      'server_software' => $_SERVER['SERVER_SOFTWARE'],
      'free_disk_space' => calculate_size(disk_free_space('/')),
      'total_disk_space' => calculate_size(disk_total_space('/')),
      'db_usage' => $db_usage."m",
      'php_version' => phpversion(),
      'mysql_version' => mysqli_get_server_info($mysqli),
      'os' => PHP_OS
    ),
    'database' =>
    array(
      'tables' => $tables
    )
  );

  return $return;
}

function calculate_size($Bytes)
{
  $Type=array("", "k", "m", "g", "t", "p", "e", "z", "y");
  $Index=0;
  while($Bytes>=1024)
  {
    $Bytes/=1024;
    $Index++;
  }
  return("".$Bytes.$Type[$Index]."b");
}

function get_server_memory_usage() {

  $free = shell_exec('free');
  $free = (string) trim($free);
  $free_arr = explode("\n", $free);
  $mem = explode(" ", $free_arr[1]);
  $mem = array_filter($mem);
  $mem = array_merge($mem);
  $memory_usage = $mem[2] / $mem[1] * 100;

  return $memory_usage;
}

function get_server_cpu_usage(){

  $load = sys_getloadavg();
  return $load[0];

}

/**
* Access callback for the note resource.
*
* @param string $op
*  The operation that's going to be performed.
* @param array $args
*  The arguments that will be passed to the callback.
* @return bool
*  Whether access is given or not.
*/

function _monitoringresource_access($op) {
  global $user;

  if ($user->uid == 1) {
    return TRUE;
  }

  return FALSE;
}
