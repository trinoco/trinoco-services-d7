<?php
// settingsservice.inc

function _settingsresource_index() {
  $settings = variable_get('trinoco-services-settings', array('export' => 'false'));

  if (isset($_GET['parameters'])) {
    $recieved_settings = explode(',', $_GET['parameters']);

    foreach ($recieved_settings as $row) {
      $setting = explode(':', $row);

      $settings[$setting[0]] = $setting[1];
    }
    variable_set('trinoco-services-settings', $settings);
  }

  $settings = array('settings' => $settings);

  return trinoco_services_utf8_converter($settings);
}

/**
* Access callback for the note resource.
*
* @param string $op
*  The operation that's going to be performed.
* @param array $args
*  The arguments that will be passed to the callback.
* @return bool
*  Whether access is given or not.
*/

function _settingsresource_access($op) {
  global $user;

  if ($user->uid == 1) {
    return TRUE;
  }

  return FALSE;
}
