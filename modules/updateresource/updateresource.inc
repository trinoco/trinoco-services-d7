<?php
// updateresource.inc

function _updateresource_index($page, $parameters) {
  $updates = trinoco_services_scan_modules();

  return trinoco_services_utf8_converter($updates);
}

/**
* Access callback for the note resource.
*
* @param string $op
*  The operation that's going to be performed.
* @param array $args
*  The arguments that will be passed to the callback.
* @return bool
*  Whether access is given or not.
*/

function _updateresource_access($op) {
  global $user;
 
  return user_access('administer software updates', $user);
}
?>