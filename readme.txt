Plaats dit project als trinoco_services bij projecten in de trinoco / custom module directore.

Of voeg dit toe aan de make file:

;Trinoco services
; --------
projects[trinoco_services][type] = "module"
projects[trinoco_services][subdir] = "trinoco_services"
projects[trinoco_services][download][type] = "get"
projects[trinoco_services][download][url] = "https://bitbucket.org/trinoco/trinoco-services-d7/get/master.zip"

Als project geupload is graag de module Trinoco services aanzetten en de volgende dingen mailen naar danny@trinoco.nl.

Url van de site
admin username
admin password

Of bij een niet make project voeg je deze repository toe als submodule:

git submodule add git@bitbucket.org:trinoco/trinoco-services-d7.git sites/all/modules/trinoco_services

Daarna op de externe omgeving de volgende commando's uitvoeren om te pullen:

git submodule init
git submodule update
